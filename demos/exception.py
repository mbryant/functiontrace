#!/usr/bin/env python3
def main():
    foo()


def foo():
    bar()


def bar():
    assert False


if __name__ == "__main__":
    main()
