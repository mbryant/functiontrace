#!/usr/bin/env python3
import subprocess
import os


def foo(x):
    return x / 2 if x % 2 == 0 else x + 1


if __name__ == "__main__":
    print("Here in subshell.py")
    foo(12345679)

    directory = os.path.dirname(__file__)
    subprocess.run(["python3", os.path.join(directory, "fib.py"), "nested"])
    subprocess.run(["python3", os.path.join(directory, "multi_thread.py"), "nested"])

    print("Back in subshell.py")
    foo(123456789)
