# https://gitlab.com/mbryant/functiontrace/-/issues/22
from threading import Thread


def fn():
    pass


Thread(target=fn).start()
