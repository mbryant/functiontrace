#!/usr/bin/env python3
from http.server import *
import http.client
import random


def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    # Make it possible to run multiple instances of this concurrently
    port = random.randint(4000, 64000)

    server_address = ("", port)
    httpd = server_class(server_address, handler_class)

    for _ in range(10):
        conn = http.client.HTTPConnection("localhost", port)
        conn.request("GET", "/")
        httpd.handle_request()


if __name__ == "__main__":
    run()
