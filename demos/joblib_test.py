#!/usr/bin/env python3
from joblib import Parallel, delayed


def work(a, b):
    return a + b


def main():
    length = 100
    res = Parallel(n_jobs=-1)(
        delayed(work)(a, b) for a, b in zip(range(length), range(length))
    )
    print(res)


if __name__ == "__main__":
    main()
