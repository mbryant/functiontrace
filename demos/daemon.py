#!/usr/bin/env python3
import threading
import time
import os


def foo(x):
    return x


def burn():
    z = 0
    for x in range(1024):
        for y in range(128):
            z += foo(1)
        time.sleep(0)


def fn():
    time.sleep(0)
    burn()


for _ in range(7):
    t = threading.Thread(target=fn)
    t.daemon = True
    t.start()

os._exit(0)

# Generate some events to fill up the buffer
burn()

time.sleep(5)
