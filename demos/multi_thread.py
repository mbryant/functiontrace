# This example is from
# https://docs.python.org/dev/library/concurrent.futures.html#threadpoolexecutor
import concurrent.futures
import urllib.request

URLS = [
    "https://www.foxnews.com/",
    "https://www.cnn.com/",
    "https://europe.wsj.com/",
    "https://www.bbc.co.uk/",
    "http://nonexistant-subdomain.python.org/",
    "https://programsareproofs.com/this-page-is-a-404.html",
]


# Retrieve a single page and report the URL and contents
def load_url(url):
    with urllib.request.urlopen(url, timeout=5) as conn:
        return conn.read()


# We can use a with statement to ensure threads are cleaned up promptly
with concurrent.futures.ThreadPoolExecutor(max_workers=len(URLS)) as executor:
    # Start the load operations and mark each future with its URL
    future_to_url = {executor.submit(load_url, url): url for url in URLS}
    for future in concurrent.futures.as_completed(future_to_url):
        url = future_to_url[future]
        try:
            data = future.result()
        except Exception as exc:
            print("%r generated an exception: %s" % (url, exc))
        else:
            print("%r page is %d bytes" % (url, len(data)))
