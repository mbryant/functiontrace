#!/usr/bin/env python3
# Time how long it times to run each demo.  Additionally ensure that the demo
# returns a sane exit code.
from pathlib import Path
import concurrent.futures
import json
import os
import platform
import subprocess
import tempfile
import time
import traceback
import shutil
import sys

# This takes much longer on Gitlab's weak CI runners than locally.
SERVER_TIMEOUT = 120 if os.environ.get("CI_JOB_ID") is not None else 20


# Run the given demo script and verify that it generates an output.
def run_script(demo_script, functiontrace_args):
    # Create an tmp directory for the test artifacts
    tmpDir = Path(tempfile.mkdtemp(prefix="functiontrace-server-test_"))

    with (tmpDir / "output.txt").open("w") as outputFile:
        start = time.time()
        # Capture extra debug logging to make tracking down test failures easier
        os.environ["RUST_LOG"] = "info"
        # Disable the gzip portion of tests, since it adds a substantial amount
        # of time to Gitlab CI execution and is mainly useful when you care
        # about disk space or need to send profiles elsewhere.
        os.environ["FUNCTIONTRACE_COMPRESSION"] = "disable"

        script_process = subprocess.Popen(
            [
                sys.executable,
                "-m",
                "functiontrace",
                "--output_dir",
                tmpDir,
                *functiontrace_args,
                str(demo_script.resolve()),
            ],
            cwd=tmpDir,
            stdout=outputFile,
            stderr=subprocess.STDOUT,
        )

    result = {
        "tmpDir": tmpDir,
    }
    try:
        script_process.wait(SERVER_TIMEOUT)
        result["client_time"] = time.time() - start
    except subprocess.TimeoutExpired:
        script_process.kill()
        result["script_fail"] = "Timeout"
        return result

    if script_process.returncode:
        # exception.py exits with returncode 1 on purpose
        if "exception.py" != demo_script.name:
            result["script_fail"] = script_process.returncode
            return result

    # Arbitarily wait a bit for the server to output the profile file
    profile = tmpDir / "functiontrace.latest.json"
    for _ in range(SERVER_TIMEOUT * 10):
        try:
            with profile.open("r") as f:
                # Validate that the profile loads as valid json
                json.load(f)
            break
        except FileNotFoundError:
            time.sleep(0.1)
    else:
        result["server_fail"] = None
        return result

    result["server_time"] = time.time() - result["client_time"] - start
    # TODO: tensor.py sometimes fails to remove everything in CI.
    shutil.rmtree(tmpDir, ignore_errors=True)
    return result


def main():
    demoDir = Path(__file__).parent / "demos"
    serverPath = Path(__file__).parent / "target" / "release" / "functiontrace-server"
    assert serverPath.exists(), serverPath
    os.environ["PATH"] = (
        str(serverPath.parent.resolve()) + os.pathsep + os.environ["PATH"]
    )

    exceptions = []

    failed = False
    test_times = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=os.cpu_count()) as executor:
        future_demos = {}
        for demo_script in [
            x for x in demoDir.iterdir() if x.is_file() and x.suffix == ".py"
        ]:
            future_demos[
                executor.submit(run_script, demo_script, [])
            ] = demo_script.name
            future_demos[
                executor.submit(run_script, demo_script, ["--trace-memory"])
            ] = "{} (mem traced)".format(demo_script.name)

        for future in concurrent.futures.as_completed(future_demos.keys()):
            test_name = future_demos[future]

            try:
                result = future.result()
            except Exception:
                exceptions.append((test_name, traceback.format_exc()))
                continue

            test_times[test_name] = (
                result.get("client_time"),
                result.get("server_time"),
            )
            if set(result.keys()) == set(["tmpDir", "client_time", "server_time"]):
                # Success
                continue

            if not failed:
                print(f"===== TEST FAILURES {'='*36}")

            # Failure cases
            failed = True

            if "script_fail" in result:
                print(f"FAIL: {test_name} returned {result['script_fail']}")
                test_times[test_name] = ()
            elif "server_fail" in result:
                print(
                    f"TIMEOUT: {test_name} - functiontrace.latest.json does not exist after {SERVER_TIMEOUT} seconds after script exited"
                )
            print(f"Output saved in: {result['tmpDir']}")

    if failed:
        print()

    if len(exceptions) > 0:
        for test, stacktrace in exceptions:
            print(f"TEST HARNESS FAILURE: {test} threw {stacktrace}")
        sys.exit(1)

    print(f"==== Results {platform.python_version():<8} {'='*34}")
    print(
        "{script:<30}  {client_time:<11}  {server_time:<11}".format(
            script="Test", client_time="Client Time", server_time="Server Time"
        )
    )

    def truncate_time(time):
        return "{:.5f}".format(time) if time else "TIMEOUT"

    for script, test_time in sorted(test_times.items()):
        if test_time == ():
            print(f"{script:<30}  {'FAIL':>11}")
        else:
            print(
                "{script:<30}  {client_time:>11}  {server_time:>11}".format(
                    script=script,
                    client_time=truncate_time(test_time[0]),
                    server_time=truncate_time(test_time[1]),
                )
            )

    print(
        "{script:<30}  {client_time:>11}  {server_time:>11}".format(
            script="Total",
            client_time=truncate_time(sum(x[0] for x in test_times.values())),
            server_time=truncate_time(sum(x[1] for x in test_times.values())),
        )
    )

    sys.exit(1 if failed else 0)


if __name__ == "__main__":
    main()
