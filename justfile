# Choose between the other options
default:
    @just --choose --justfile {{justfile()}}

# Build functiontrace-server
build-server:
    cargo build --release

# Build py-functiontrace
build-python:
    #!/usr/bin/sh
    cd py-functiontrace
    python3.11 -m build

test: build-server build-python
    python3.11 integration_tests.py

# Deploy functiontrace-server to crates.io
deploy-server: test
    cargo publish

# Deploy py-functiontrace to pypi
deploy-pypi artifacts_zip: test
    # NOTE: You must first download artifacts from Gitlab
    rm -rf /tmp/py-functiontrace_dist/
    unzip -j -d /tmp/py-functiontrace_dist/ {{artifacts_zip}}
    python3.11 -m twine upload /tmp/py-functiontrace_dist/functiontrace-*-manylinux*.whl
    python3.11 -m twine upload /tmp/py-functiontrace_dist/functiontrace-*.tar.gz
    rm {{artifacts_zip}}
