See https://functiontrace.com/ for end user instructions and demos.

Functiontrace is implemented as two components:
1. A server, written in Rust, which aggregates trace information from running
   processes
2. A python module, written in C and Python, which hooks in to the various
   traced events.

# Development Info

## Developing with Nix

With Nix, we use `devenv` to have a standard test and development environment.
The standard development loop is to setup that environment using `devenv shell`.

```sh
devenv shell
just test
```

You can test small changes inside an existing `devenv shell`, including working
with most of our supported Python variants.  To rebuild the Python module, you
need to tell Nix to rebuild by restarting your `devenv shell`.

## Developing with standard Python

### Building the server
```
$ just build-server
```

### Building the python module
```
$ just build-python
$ python setup.py bdist_wheel --dist-dir ./dist

# You may also want to install it
$ pip install py-functiontrace/dist/*.whl
```

## Testing
You must have `functiontrace` installed, and have `functiontrace-server` in
your `$PATH`.  The Nix development path should do this for you.
```
# The test script will use the server built in target/release
$ just test
```
