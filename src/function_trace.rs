use serde::Deserialize;
use std::borrow::Cow;
use std::num::NonZeroU32;
use std::time::Duration;

/// A representation of the various trace messages a client thread can send us during normal
/// tracing operation.
///
/// We use [`Cow`] instead of [`String`] because most strings should be zero-copy deserializable.
#[derive(Deserialize, Debug)]
#[serde(tag = "type")]
pub enum FunctionTrace<'event> {
    /// A request to register this as a new thread.
    ///
    /// **NOTE**: This must be sent as the first message from a new thread, and may not be sent
    /// twice by any thread.
    RegisterThread(ThreadRegistration),
    Call {
        time: Duration,
        #[serde(borrow)]
        func_name: Cow<'event, str>,
        #[serde(borrow)]
        filename: Cow<'event, str>,
        linenumber: NonZeroU32,
    },
    Return {
        time: Duration,
        #[serde(borrow)]
        func_name: Cow<'event, str>,
    },
    NativeCall {
        time: Duration,
        #[serde(borrow)]
        func_name: Cow<'event, str>,
        #[serde(borrow)]
        module_name: Cow<'event, str>,
    },
    NativeReturn {
        time: Duration,
        #[serde(borrow)]
        func_name: Cow<'event, str>,
    },
    Exception {
        time: Duration,
        #[serde(borrow)]
        exception_type: Cow<'event, str>,
        #[serde(borrow)]
        exception_value: Cow<'event, str>,
        filename: String,
        linenumber: NonZeroU32,
    },
    Log {
        time: Duration,
        #[serde(borrow)]
        log_type: Cow<'event, str>,
        #[serde(borrow)]
        log_value: Cow<'event, str>,
    },
    Import {
        time: Duration,
        #[serde(borrow)]
        module_name: Cow<'event, str>,
    },
    Allocation {
        time: Duration,
        details: AllocationDetails,
    },
}

/// Information about allocations.
#[derive(Deserialize, Debug)]
#[serde(tag = "type")]
pub enum AllocationDetails {
    /// The amount and location of a new allocation
    Alloc { bytes: usize, addr: usize },
    /// The new size of a reallocation from `old_addr` to `new_addr`.
    Realloc {
        bytes: usize,
        old_addr: usize,
        new_addr: usize,
    },
    /// The address that was `free()`ed.
    Free { old_addr: usize },
}

/// Information relevant for initializing a trace.
#[derive(Deserialize, Debug)]
pub struct TraceInitialization {
    /// The name (typically based off of argv) of the program initializing the trace (ex:
    /// `hello.py world`).
    pub program_name: String,
    /// The version information of the `functiontrace` client talking to this server (ex:
    /// `py-functiontrace 0.3.0`).
    pub program_version: String,
    /// The version for the underlying language the program is running on (ex: `Python 3.7.1`).
    pub lang_version: String,
    /// The general operating system platform the program is running on (ex: `darwin`).
    pub platform: String,
    /// An opaque system time that all other client-sent times will be relative to.
    pub time: Duration,
}

/// This message contains information for registering threads (including a program's main thread).
#[derive(Deserialize, Debug)]
pub struct ThreadRegistration {
    /// A system time for when this thread registered itself as being traced with FunctionTrace.
    pub time: Duration,
    /// The program name this thread corresponds to.
    pub program_name: String,
    /// The process this thread belongs to.
    pub pid: usize,
}
