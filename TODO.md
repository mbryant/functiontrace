Key Features
============
- [x] Multiprocessing support
- [x] Multithreading support
- [x] Subprocess hooking support
- [ ] Class method hooking
- [x] <30% overhead

Nice to have
=============
- [x] Argument passing from Python -> C
- [x] Client-side configuration of where to write output files
- [x] Auto-spawning server
- [ ] Trace GC
- [x] Trace prints/logging
- [x] Trace memory allocations
- [ ] Trace syscalls
- [ ] Trace network calls
- [ ] Trace subprocess.Popen calls
- [ ] Log exceptions nicely

Monetization Ideas
==================
- [ ] Coverage/execution paths
- [ ] Automated checkers
