{ pkgs, lib, ... }:

let
  # Setup a Python environment for the specified python interpreter
  #   python: the python interpreter to use
  #   extra_deps: (pkgs -> [ package list to install])
  pythonEnv =
    python: extra_deps:
    (python.override {
      enableOptimizations = true;
      packageOverrides = self: super: {
        functiontrace = self.callPackage (
          {
            buildPythonPackage,
            toml,
            setuptools,
            ...
          }:
          buildPythonPackage rec {
            name = "functiontrace";
            src = lib.cleanSource ./py-functiontrace;
            format = "pyproject";

            pythonImportsCheck = [ "functiontrace" ];
            doCheck = false;
            propagatedBuildInputs = [
              toml
              setuptools
            ];
          }
        ) { };
      };
    }).withPackages
      (
        ps:
        with ps;
        [
          # Required modules for building and working with functiontrace
          pip
          wheel
          setuptools
          twine
          build
          toml
          functiontrace

          # Test dependencies
          joblib
          pyyaml
        ]
        ++ extra_deps (ps)
      );
in
{
  packages = with pkgs; [
    # Core Python environments
    # NOTE: The ML modules often don't work properly under Nix, so we install
    # them manually in each env we care about.
    # - keras
    # - tensorflow
    (pythonEnv python310 (ps: [
      ps.keras
      ps.tensorflow
    ]))
    (pythonEnv python311 (ps: [
      ps.keras
      ps.tensorflow
    ]))
    (pythonEnv python312 (ps: [ ]))

    # Dev tools
    just
    unzip
  ];

  enterShell = ''
    # Ensure functiontrace-server is in the path
    export PATH="$PWD/target/release/:$PATH"
  '';

  # https://devenv.sh/languages/
  languages.rust.enable = true;
}
